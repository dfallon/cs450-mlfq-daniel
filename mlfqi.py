# iproved MLFQ scheduler implementation

from collections import deque

class MLFQScheduler:
    def __init__(self, quanta):
        #create and populate Job Queues
        self.jq = self.JobQueue(quantum = quanta[0])
        self.jqs = list()
        for curQuantum in quanta[1:]:
            self.jq.insertJobQueue(curQuantum)
        while self.jq.hp is not None:
            self.jq = self.jq.hp
        i = self.jq
        while i is not None:
            self.jqs.append(i)
            i = i.lp
        self.jiq = dict()
        self.currJob = None
        return

    # called when a job is first created -- the job is assumed
    # to be ready at this point (note: job_ready will not be called
    # for a job's first CPU burst)
    def job_created(self, jid):
        self.jiq[jid] = self.Job(jid,self.jq,self.jqs)
        self.jiq[jid].enqueue()
        return

    # called when a job becomes ready after an I/O burst completes
    def job_ready(self, jid):
        self.jiq[jid].enqueue()
        return

    # called when a job's current CPU burst runs to the end of its
    # allotted time quantum without completing -- the job is
    # still in the ready state
    def job_quantum_expired(self, jid):
        self.jiq[jid].demote()
        self.jiq[jid].enqueue()
        return
    
    # called when a job is preempted mid-quantum due to our
    # returning True to `needs_resched` -- the job is still in
    # the ready state
    def job_preempted(self, jid):
        self.jiq[jid].enqueue()
        return

    # called after a job completes its final CPU burst -- the job
    # will never become ready again
    def job_terminated(self, jid):
        if self.currJob.jid == jid:
            self.currJob = None
        return

    # called when a job completes its CPU burst within the current
    # time quantum and has moved into its I/O burst -- the job is
    # currently blocked
    def job_blocked(self, jid):
        if self.currJob.jid == jid:
             self.currJob = None
        return

    # called by the simulator after new jobs have been made ready.
    # we should return True here if we have a more deserving job and
    # want the current job to be preempted; otherwise return False
    def needs_resched(self):
        currJq = self.jq
        while currJq is not None and currJq is not self.currJob.jq:
            if currJq:
                return True
            currJq = currJq.lp
        return False

    # return a two-tuple containing the job ID and time quantum for
    # the next job to be scheduled; if there is no ready job,
    # return (None, 0)
    def next_job_and_quantum(self):
        currJq = self.jq
        while currJq is not None:
            try:
                job = currJq.popleft()
            except IndexError:
                job = None
            if job is not None:
                self.currJob = job
                return (job.jid, currJq.quantum)
            currJq = currJq.lp
        return (None, 0)

    # called by the simulator after all jobs have terminated -- we
    # should at this point compute and print out well-formatted
    # scheduler statistics
    def print_report(self):
        #do calculations
        qlist = dict()
        totalJs = 0
        for jid, job in self.jiq.iteritems():
            for quantum, length in job.jqStartLen.iteritems():
                try:
                    qlist[quantum]+=length
                except KeyError:
                    qlist[quantum]=length
            totalJs+=1
        for quantum, length in qlist.iteritems():
            qlist[quantum]/=float(totalJs)
        #start printing
        print("\n  JID | # Switches\n------------------")
        for jid, job in self.jiq.iteritems():
            print("{:5d} | {:10d}".format(job.jid, job.switches))

        print("\n\nQueue | Avg length\n------------------")
        for queue in sorted(qlist.keys()):
                print("{:5d} | {:10f}".format(queue, qlist[queue]))
        return

    class JobQueue(deque):
        def __init__(self, *args, **kwargs):
            self.quantum = kwargs.get("quantum")
            self.lp = kwargs.get("lowerPri")
            self.hp = kwargs.get("higherPri")
            super(self.__class__, self).__init__()

        def insertJobQueue(self,quantum,prev=None):
            if quantum == self.quantum and quantum > 0:
                return
            elif quantum < self.quantum:
                if self.hp is None:
                    self.hp = self.__class__(quantum=quantum, lowerPri = self)
                elif prev is None:
                        self.hp.insertJobQueue(quantum,self)
                elif prev is self.hp:
                            self.hp = self.__class__(quantum=quantum, lowerPri = self, higherPri = prev)
                            self.hp.hp.lp = self.hp
                else:
                    self.hp.insertJobQueue(quantum, self)

            else:
                if self.lp is None:
                    self.lp = self.__class__(quantum=quantum, higherPri = self)
                elif prev is None:
                    self.lp.insertJobQueue(quantum,self)
                elif prev is self.lp:
                    self.lp = self.__class__(quantum = quantum, higherPri = self, lowerPri = prev)
                    self.lp.lp.hp = self.lp
                else:
                    self.lp.insertJobQueue(quantum,self)

    class Job:
        def __init__(self, jid, jq, jqs):
            self.jid = jid
            self.jq = jq
            self.switches = 0
            self.jqStartLen = dict({q.quantum:len(q) for q in jqs})
        def demote(self):
            if self.jq.lp is not None:
                self.jq = self.jq.lp
                self.switches+=1
        def promote(self):
            if self.jq.hp is not None:
                self.jq = self.jq.hp
                self.switches+=1
        def enqueue(self):
            self.jq.append(self)
